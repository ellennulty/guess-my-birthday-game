from random import randint

name = input("Hi! What is your name?")

for guess in range(5):

    month_guess = randint(1, 12)
    year_guess = randint(1924, 2004)

    print("Guess", guess + 1, name, "were you born in", month_guess, year_guess, "?")

    answer = input("yes or no?")
    if answer == "Yes":
        print("I knew it!")
        break
    elif guess < 4:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
